package com.zongtui.fourinone.base;

import com.zongtui.fourinone.park.ParkActive;
import com.zongtui.fourinone.park.ParkManager;
import com.zongtui.fourinone.utils.log.LogUtil;

import java.rmi.RemoteException;

/**
 * 服务上下文.【继承基础bean的putbean操作。】
 */
public class ServiceContext extends BeanService {

    /**
     * 启动无策略的服务.
     *
     * @param host        主机号
     * @param port        端口号
     * @param serviceName 服务名
     * @param object      对象
     * @param <I>         对象泛型，必须继承ParkActive对象.
     */
    public static <I extends ParkActive> void startService(String host, int port, String serviceName, I object) {
        try {
            //默认是没有logCall的。
            putBean(host, false, port, serviceName, object);
        } catch (Exception e) {
            LogUtil.info("[ObjectService]", "[startService]", e);
//            e.printStackTrace();
        }
    }

    /**
     * 启动有策略的服务.
     *
     * @param host        主机号
     * @param port        端口号
     * @param serviceName 服务名
     * @param object      对象
     * @param <I>         对象泛型，必须继承ParkActive对象.
     * @param codebase    codebase
     * @param policy      策略类名
     */
    public static <I extends ParkActive> void startService(String host, int port, String serviceName, I object, String codebase, String policy) {
        try {
            //默认是没有logCall的。
            putBean(host, false, port, serviceName, object, codebase, policy, new ParkManager());
        } catch (Exception e) {
            LogUtil.info("[ObjectService]", "[startService]", e);
        }
    }

    /**
     * 获得远程服务对象.
     *
     * @param clazz       对象类型
     * @param host        主机号
     * @param port        端口号
     * @param serviceName 服务名
     * @param <I>         泛型
     * @return 返回该类型的远程对象.
     */
    public static <I extends ParkActive> I getService(Class<I> clazz, String host, int port, String serviceName) {
        I i = null;
        try {
            i = (I) getBean(host, port, serviceName);
        } catch (RemoteException e) {
            LogUtil.info("[ObjectService]", "[getService]", e);
        }
        return i;
    }
}